# desafio-teste-api

Repositório referente ao desafio de teste de api, do programa de Capacitação DBServer/Banrisul

## Sobre o desafio

O objetivo do desafio consiste em desenvolver testes baseados em diferentes cenários, por meio da api de serviço do [**viacep.com.br**](https://viacep.com.br/). Os cenários são consultas de um CEP válido, um CEP inválido e um CEP inexistente no banco dos Correios.
Além disso, o desafio possui um exercício extra cujo objetivo é cria um cenário que verifique o retorno do serviço presente no [**link**](https://viacep.com.br/ws/RS/Gravatai/Barroso/json).

## Tecnologias utilizadas

Java, JUnit, Gradle, JSON e Allure.

## Ferramenta de desenvolvimento

O projeto foi feito utilizando a ferramenta Eclipse, para desenvolvimento em Java.

## Distribuição do trabalho

O projeto é distribuido em diferentes pacotes referentes aos diferentes cenários do desafio:

## Principais pacotes
### Pacote testeBase

* #### testBase.viaCep.valido
    * Possui duas classes de consulta a um CEP de formato válido (8 caracteres, todos numéricos).
    * Das duas classes, uma corresponde a presença de input do usuário, enquanto a outra consiste em um CEP previamente informado.

* #### testBase.viaCep.invalido
    * Possui duas classes de consulta a um CEP de formato inválido (qualquer configuração que não seja a descrita anteriormente).
    * Das duas classes, uma corresponde a presença de input do usuário, enquanto a outra consiste em um CEP previamente informado.

* #### testBase.viaCep.inexistente
    * Possui duas classes de consulta a um CEP que não existe no banco dos Correios.
    * Das duas classes, uma corresponde a presença de input do usuário, enquanto a outra consiste em um CEP previamente informado.

* #### testBase.viaCep.extra
    * Possui a classe de consulta dos cenário proposto para o exercício extra.

### Pacote testCase

Assim como no pacote testBase, foram feitas duas classes para cada cenário, correspondendo à ausência ou à presença de input do usuário.

* #### testCase.viaCep.valido
    * Possui os testes para consulta de um CEP válido.

* #### testCase.viaCep.invalido
    * Possui os testes para consulta de um CEP inválido.

* #### testCase.viaCep.inexistente
    * Possui os testes para consulta de um CEP inexistente no banco dos Correios.

* #### testCase.viaCep.extra
    * Possui o teste para consulta do cenário do exercício extra.
