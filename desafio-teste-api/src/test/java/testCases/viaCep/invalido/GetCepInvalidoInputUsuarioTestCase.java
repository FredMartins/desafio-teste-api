package testCases.viaCep.invalido;

import static io.restassured.RestAssured.given;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.restassured.response.Response;
import testBase.viaCep.invalido.CepInvalidoInputUsuarioTestBase;

public class GetCepInvalidoInputUsuarioTestCase extends CepInvalidoInputUsuarioTestBase{
	
	@Test
	public void cepInvalido() {
		Response payload =
		given()
				.spec(requestSpec)
		.when()
				.get()
		.then()
				.spec(responseSpec).extract().response();
		
		buildErrorMessage(payload);
		Assertions.assertEquals("Erro 400", errorModel.getTitle());
		
		System.out.println("\n"+errorModel);
	}
}
