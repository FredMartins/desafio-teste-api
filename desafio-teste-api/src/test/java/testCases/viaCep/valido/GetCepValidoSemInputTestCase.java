package testCases.viaCep.valido;

import static io.restassured.RestAssured.given;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.restassured.response.Response;
import testBase.viaCep.valido.CepValidoSemInputTestBase;

public class GetCepValidoSemInputTestCase extends CepValidoSemInputTestBase{
	@Test
	public void cepValidoSemInput(){
		Response payload =
		given()
				.spec(requestSpec)
		.when()
				.get()
		.then()
				.spec(responseSpec).extract().response();
		
		buildPojoObject(payload);
		Assertions.assertNotEquals(null, cepModel.getCep(),"CEP n�o existe no banco dos Correios!");
		System.out.println("CEP: "+cepModel.getCep()+"\n"+
							"Logradouro: "+cepModel.getLogradouro()+"\n"+
							"Complemento: "+cepModel.getComplemento()+"\n"+
							"Bairro: "+cepModel.getBairro()+"\n"+
							"Localidade: "+cepModel.getLocalidade()+"\n"+
							"UF: "+cepModel.getUf()+"\n"+
							"IBGE: "+cepModel.getIbge());
	}
}
