package testCases.viaCep.inexistente;

import static io.restassured.RestAssured.given;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.restassured.response.Response;
import testBase.viaCep.inexistente.CepInexistenteInputUsuarioTestBase;

public class GetCepInexistenteInputUsuarioTestCase extends CepInexistenteInputUsuarioTestBase{
	
	@Test
	public void cepInexistente() {
		Response payload=
		given()
				.spec(requestSpec)
		.when()
				.get()
		.then()
				.spec(responseSpec).extract().response();
		
		buildPojoObject(payload);
		
		Assertions.assertEquals(null,cepModel.getCep(),"CEP informado existe no banco dos Correios");
		boolean erro = payload.then().extract().path("erro");
		
		System.out.println("Erro: "+erro);
		if(erro) {
			System.out.println("CEP "+userCep+" n�o encontrado!");
		}
	}
}
