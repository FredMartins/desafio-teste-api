package testCases.viaCep;

import static io.restassured.RestAssured.given;

import org.junit.jupiter.api.Test;

import testBase.viaCep.SimpleTestBase;

public class GetNewSimpleTestCase extends SimpleTestBase{
	
	@Test
	public void simpleTestCase() {
		given()
				.spec(requestSpec)
				.log().all()
		.when()
				.get()
		.then()
				.log().all()
				.spec(responseSpec);
	}
}
