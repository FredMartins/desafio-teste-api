package testCases.viaCep.extra;

import static io.restassured.RestAssured.given;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.restassured.response.Response;
import testBase.viaCep.extra.RetornoExtraTestBase;

public class GetRetornoExtraTestCase extends RetornoExtraTestBase{
	
	@Test
	public void retornoExtra() {
		Response payload =
		given()
				.spec(requestSpec)
		.when()
				.get()
		.then()
				.spec(responseSpec).extract().response();
		buildArrayListOfPojoObject(payload);
		
		Assertions.assertTrue(cepModels.size()>0,"N�o foram encontrados resultados para sua consulta!");
		System.out.println("Resultados encontrados:\n");
		printCompleto();
	}
}
