package models;

public class ErrorModel {
	private String title;
	private String subtitle;
	private String message;
	public ErrorModel(String title, String subtitle, String message) {
		this.title = title;
		this.subtitle = subtitle;
		this.message = message;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "" + title + "\n" + subtitle + "\n" + message;
	}
	
	
}
