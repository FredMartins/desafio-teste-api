package testBase.viaCep.extra;

import java.util.ArrayList;
import java.util.Map;
import org.junit.jupiter.api.BeforeAll;

import Utils.FileOperations;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import models.CepModel;

public class RetornoExtraTestBase {
	protected static RequestSpecification requestSpec;
	protected static ResponseSpecification responseSpec;
	private static String baseUri = FileOperations.getProperties("viacep").getProperty("baseuri");
	private static String basePath = FileOperations.getProperties("viacep").getProperty("basepath");
	protected static ArrayList<CepModel> cepModels = new ArrayList<>();
	
	@BeforeAll
	public static void setUp() {
		buildRequestSpec();
		buildResponseSpec();
	}
	
	public static void buildRequestSpec() {
		requestSpec = new RequestSpecBuilder()
				.setBaseUri(baseUri)
				.setBasePath("/ws/"+basePath+"/json")
				.build();
	}
	
	public static void buildResponseSpec() {
		responseSpec = new ResponseSpecBuilder()
				.expectStatusCode(200)
				.expectContentType(ContentType.JSON)
				.build();
	}
	
	public static void buildArrayListOfPojoObject(Response payload) {
		ArrayList<Map<String, String>> list = payload.then().extract().jsonPath().get();
		for (Map<String, String> map : list) {
			CepModel cepModelAux = new CepModel(map.get("cep"),
							   map.get("logradouro"),
							   map.get("complemento"),
							   map.get("bairro"),
							   map.get("localidade"),
							   map.get("uf"),
							   map.get("ibge"),
							   map.get("gia"),
							   map.get("ddd"),
							   map.get("siafi"));
			cepModels.add(cepModelAux);
		}
	}
	
	public static void printCompleto() {
		for (CepModel cepModel : cepModels) {
			System.out.println(cepModel.toStringCompleto());
		}
	}
}
