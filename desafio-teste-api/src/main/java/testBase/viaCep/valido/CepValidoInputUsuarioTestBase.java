package testBase.viaCep.valido;

import java.util.Scanner;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;

import Utils.FileOperations;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import models.CepModel;

public class CepValidoInputUsuarioTestBase {
	protected static RequestSpecification requestSpec;
	protected static ResponseSpecification responseSpec;
	private static String baseUri = FileOperations.getProperties("viacep").getProperty("baseuri");
	private static String validCep;
	private static String userCep;
	protected static CepModel cepModel;
	
	@BeforeAll
	public static void setUp() {
		setUpCepProperties();
		buildRequestSpec();
		buildResponseSpec();
	}
	
	public static void buildRequestSpec() {
		requestSpec = new RequestSpecBuilder()
				.setBaseUri(baseUri)
				.setBasePath("/ws/"+validCep+"/json")
				.build();
	}
	
	public static void buildResponseSpec() {
		responseSpec = new ResponseSpecBuilder()
				.expectStatusCode(200)
				.expectContentType(ContentType.JSON)
				.build();
	}
	
	
	/*
	 * Usuario insere o Cep desejado. Deve ser uma String contendo apenas n�mero.
	 * O m�todo deve alterar o valor da chave "validocep" em "viacep.properties".
	 */
	public static void setUpCepProperties() {
		System.out.println("Insira o Cep desejado (APENAS N�MEROS):");
		Scanner in = new Scanner(System.in);
		do {
			userCep = in.next();
			if(!verificaCepValido()) {
				System.out.println("Cep inv�lido! Tente novamente!");
			}
		} while (!verificaCepValido());
		//userCep = in.nextLine();
		//Assertions.assertTrue(verificaCepValido(), "Formato do CEP inv�lido!");
		FileOperations.setProperties("viacep", "validocep", userCep);
		validCep = FileOperations.getProperties("viacep").getProperty("validocep");
	}
	
	/*
	 * Verifica se o CEP informado tem o tamanho indicado e � composto apenas por n�meros.
	 */
	public static boolean verificaCepValido() {
		if(userCep.length() < 8 || userCep.length() > 8) {
			return false;
		}
		for(int i = 0; i < userCep.length(); i++) {
			if(userCep.charAt(i) < 48 || userCep.charAt(i) > 57) {
				return false;
			}
		}
		return true;
	}
	
	public static void buildPojoObject(Response payload) {
		cepModel = new CepModel(payload.then().extract().path("cep"), 
								payload.then().extract().path("logradouro"), 
								payload.then().extract().path("complemento"), 
								payload.then().extract().path("bairro"), 
								payload.then().extract().path("localidade"), 
								payload.then().extract().path("uf"), 
								payload.then().extract().path("ibge"));
	}
	
	
}
