package testBase.viaCep.invalido;

import org.junit.jupiter.api.BeforeAll;

import Utils.FileOperations;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import models.ErrorModel;

public class CepInvalidoSemInputTestBase {
	protected static RequestSpecification requestSpec;
	protected static ResponseSpecification responseSpec;
	private static String baseUri = FileOperations.getProperties("viacep").getProperty("baseuri");
	protected static String cep = FileOperations.getProperties("viacep").getProperty("invalidocep2");
	protected static ErrorModel errorModel;
	
	@BeforeAll
	public static void setUp() {
		buildRequestSpec();
		buildResponseSpec();
	}
	
	public static void buildRequestSpec() {
		requestSpec = new RequestSpecBuilder()
				.setBaseUri(baseUri)
				.setBasePath("/ws/"+cep+"/json")
				.build();
	}
	
	public static void buildResponseSpec() {
		responseSpec = new ResponseSpecBuilder()
				.expectStatusCode(400)
				.expectContentType(ContentType.HTML)
				.build();
	}
	
	public static void buildErrorMessage(Response payload) {
		errorModel = new ErrorModel(payload.then().extract().htmlPath().getString("html.body.h1"), 
									payload.then().extract().htmlPath().getString("html.body.h2"), 
									payload.then().extract().htmlPath().getString("html.body.h3"));
	}
}
