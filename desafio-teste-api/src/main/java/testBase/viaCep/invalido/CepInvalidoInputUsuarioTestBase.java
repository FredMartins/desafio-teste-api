package testBase.viaCep.invalido;

import java.util.Scanner;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;

import Utils.FileOperations;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import models.ErrorModel;

public class CepInvalidoInputUsuarioTestBase {
	protected static RequestSpecification requestSpec;
	protected static ResponseSpecification responseSpec;
	private static String baseUri = FileOperations.getProperties("viacep").getProperty("baseuri");
	private static String invalidCep;
	private static String userCep;
	protected static ErrorModel errorModel;
	
	@BeforeAll
	public static void setUp() {
		setUpCepInvalido();
		buildRequestSpec();
		buildResponseSpec();
	}
	
	public static void buildRequestSpec() {
		requestSpec = new RequestSpecBuilder()
				.setBaseUri(baseUri)
				.setBasePath("/ws/"+invalidCep+"/json")
				.build();
	}
	
	public static void buildResponseSpec() {
		responseSpec = new ResponseSpecBuilder()
				.expectStatusCode(400)
				.expectContentType(ContentType.HTML)
				.build();
	}
	
	public static void setUpCepInvalido() {
		System.out.println("Informe um CEP:");
		Scanner in = new Scanner(System.in);
		do {
			userCep = in.next();
			if(!verificaFormatoCepInvalido()) {
				System.out.println("CEP deve ter formato inv�lido!");
			}
		} while (!verificaFormatoCepInvalido());
		//userCep = in.next();
		//Assertions.assertTrue(verificaFormatoCepInvalido(),"CEP deve ter formato inv�lido!");
		FileOperations.setProperties("viacep", "invalidocep", userCep);
		invalidCep = FileOperations.getProperties("viacep").getProperty("invalidocep");
	}
	
	public static boolean verificaFormatoCepInvalido() {
		if(userCep.length() < 8 || userCep.length() > 8) {
			return true;
		}
		for(int i = 0; i< userCep.length(); i++) {
			if(userCep.charAt(i) < 48 || userCep.charAt(i) > 57) {
				return true;
			}
		}
		return false;
	}
	
	public static void buildErrorMessage(Response payload) {
		errorModel = new ErrorModel(payload.then().extract().htmlPath().getString("html.body.h1"), 
							   payload.then().extract().htmlPath().getString("html.body.h2"), 
							   payload.then().extract().htmlPath().getString("html.body.h3"));
	}
}
