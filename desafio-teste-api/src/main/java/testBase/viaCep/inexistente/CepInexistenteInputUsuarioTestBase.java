package testBase.viaCep.inexistente;

import java.util.Scanner;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;

import Utils.FileOperations;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import models.CepModel;

public class CepInexistenteInputUsuarioTestBase {
	protected static RequestSpecification requestSpec;
	protected static ResponseSpecification responseSpec;
	private static String baseUri = FileOperations.getProperties("viacep").getProperty("baseuri");
	private static String notFoundCep;
	protected static String userCep;
	protected static CepModel cepModel;
	
	@BeforeAll
	public static void setUp() {
		setUpCepInexistente();
		buildRequestSpec();
		buildResponseSpec();
	}
	
	public static void buildRequestSpec() {
		requestSpec = new RequestSpecBuilder()
				.setBaseUri(baseUri)
				.setBasePath("/ws/"+notFoundCep+"/json")
				.build();
	}
	
	public static void buildResponseSpec() {
		responseSpec = new ResponseSpecBuilder()
				.expectStatusCode(200)
				.expectContentType(ContentType.JSON)
				.build();
	}
	
	public static void setUpCepInexistente() {
		System.out.println("Insira um CEP (APENAS N�MEROS):");
		Scanner in = new Scanner(System.in);
		do {
			userCep = in.next();
			if(!verificaFormatoCep()) {
				System.out.println("Cep inv�lido! Tente novamente:");
			}
		} while (!verificaFormatoCep());
		//userCep = in.nextLine();
		//Assertions.assertTrue(verificaCepValido(), "Formato do CEP inv�lido!");
		FileOperations.setProperties("viacep", "inexistentecep", userCep);
		notFoundCep = FileOperations.getProperties("viacep").getProperty("inexistentecep");
	}
	
	public static boolean verificaFormatoCep() {
		if(userCep.length() < 8 || userCep.length() > 8) {
			return false;
		}
		for(int i = 0; i< userCep.length(); i++) {
			if(userCep.charAt(i) < 48 || userCep.charAt(i) > 57) {
				return false;
			}
		}
		return true;
	}
	
	public static void buildPojoObject(Response payload) {
		cepModel = new CepModel(payload.then().extract().path("cep"), 
								payload.then().extract().path("logradouro"), 
								payload.then().extract().path("complemento"), 
								payload.then().extract().path("bairro"), 
								payload.then().extract().path("localidade"), 
								payload.then().extract().path("uf"), 
								payload.then().extract().path("ibge"));
	}
}
