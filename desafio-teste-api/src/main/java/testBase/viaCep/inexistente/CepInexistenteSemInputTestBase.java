package testBase.viaCep.inexistente;

import org.junit.jupiter.api.BeforeAll;
import Utils.FileOperations;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import models.CepModel;

public class CepInexistenteSemInputTestBase {
	protected static RequestSpecification requestSpec;
	protected static ResponseSpecification responseSpec;
	private static String baseUri = FileOperations.getProperties("viacep").getProperty("baseuri");
	protected static String cep = FileOperations.getProperties("viacep").getProperty("inexistentecep2");
	protected static CepModel cepModel;
	
	@BeforeAll
	public static void setUp() {
		buildRequestSpec();
		buildResponseSpec();
	}
	
	public static void buildRequestSpec() {
		requestSpec = new RequestSpecBuilder()
				.setBaseUri(baseUri)
				.setBasePath("/ws/"+cep+"/json")
				.build();
	}
	
	public static void buildResponseSpec() {
		responseSpec = new ResponseSpecBuilder()
				.expectStatusCode(200)
				.expectContentType(ContentType.JSON)
				.build();
	}
	
	public static void buildPojoObject(Response payload) {
		cepModel = new CepModel(payload.then().extract().path("cep"), 
								payload.then().extract().path("logradouro"), 
								payload.then().extract().path("complemento"), 
								payload.then().extract().path("bairro"), 
								payload.then().extract().path("localidade"), 
								payload.then().extract().path("uf"), 
								payload.then().extract().path("ibge"));
	}
}
